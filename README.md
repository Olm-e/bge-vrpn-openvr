a bridge between the blender game engine and OpenVR to use tracked HMD and controllers

it uses SAT's vrpn openvr bridge in python to catch the coordinates 

tested in debian gnu-linux, may work on other system

== prerequisite : == 

building VRPN and VRPN-OpenVR bridge

https://github.com/sat-metalab/vrpn-openvr

when build, look for the vrpn.so binary and copy it into blender's bin folder (blender/2.79/python/bin/)

the main script here is the vrbridge.py used on an empty on which is parented camera and body (see demo file - to come)


== Limitation : == 

 *  for now the script only register and track the HMD orientation and position 
it can be extended to track the controllers with orientation, position and button (todo)
contribution welcome (or it will only comes when I need it ;) )

 *  this only gets the coordinates of trackers, it does not change the blender game engine renderer to pass the rendered texture to the Vive/other Headset, (this needs to be done by someone else) 
so it must be used in "extended desktop" mode. On Gnu-Linux with NVidia driver you need to configure with

sudo nvidia-xconfig --hallow-hmd true

== launch : ==

first launch Steam and SteamVR, or just the vrmonitor if you want
(it can complain that some things are not working at first due to desktop mode... don't listen ;) )

cd ~/.steam/steam/ubuntu12_32/steam-runtime 

./run.sh ../../steamapps/common/SteamVR/bin/vrmonitor.sh &

then launch the VRPN-OpenVR bridge (change path to proper vrpnOpenVR bin)

cd ~/.steam/steam/ubuntu12_32/steam-runtime 

./run.sh ~/vrpn-openvr/build/vrpnOpenVR &

finaly launch blender (or blenderplayer) with the VR scene... 


