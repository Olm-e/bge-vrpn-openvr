#############################
# this code is published under the GNU-GPL 3.0 Licence
# (c) Olivier Meunier aka Olm-e 
# 05/2018
# 
# version note
# alpha 0.1 : first publication without blend demo file - this version tracks but does not push position up to the owner's properties
# alpha 0.2 : tracking with orientation and position of HMD
#############################





from bge import logic as g
from mathutils import *
from math import *

import vrpn

scene = g.getCurrentScene()
cont = g.getCurrentController()
own = cont.owner
objects = scene.objects
Hcamera = objects['Camera']
Htranslate = objects['translate']

class vTracker(object):
    """ define a tracker class to report position and quaternion with adress as argument
    """


    def __init__(self, hmdaddr):
        self.device = vrpn.receiver.Tracker(hmdaddr)
        print(self.device)
        #device.active = True
        self.device.register_change_handler(0, self.handle_tracker, "position")

    def handle_tracker(self, userdata, t):
        #print(t)
        rot = t["quaternion"]
        pos = t["position"]
        g.hmd = (rot, pos)

    def update(self):
        self.device.mainloop()


def init():
    HMD_ADDR = "openvr/hmd/0@localhost"
    cont = g.getCurrentController()
    own = cont.owner
    scene = g.getCurrentScene()
    #cam = scene.active_camera
    objects = scene.objects
#    Hcamera = objects['translate']
    Hcamera = objects['Camera']
    g.vive = vTracker(HMD_ADDR)
    print("vr init")

def run():
    g.vive.update()
    
    hmdrot = Quaternion(g.hmd[0])#.to_matrix()
    #print(hmdrot)
    hmdrot.w = hmdrot.w
    hmdrottempx = hmdrot.x
    hmdrottempy = hmdrot.y
    hmdrottempz = hmdrot.z
    hmdrot.x = -hmdrottempz
    hmdrot.z = -hmdrottempx
    hmdrot.y = hmdrottempy
    pos = g.hmd[1]
    print(pos)
    mat_rotX = Matrix.Rotation(radians(-90), 3, 'X')
    rot = mat_rotX*hmdrot.to_matrix()
    rot_e = rot.to_euler()
#    print(rot)
#    print(rot_e)

    

    Hcamera.localOrientation = rot
    
    #ownpos = camera.localPosition
    transpos = Htranslate.localPosition
                    
    multiplier = 1.0
    transpos[0] = ( pos[0] * multiplier)
    transpos[1] = (-pos[2] * multiplier)
    transpos[2] = ( pos[1] * multiplier) 
                    
#    transpos[0] = ownpos[0] + ( pos[0] * multiplier)
#    transpos[1] = ownpos[1] + (-pos[2] * multiplier)
#    transpos[2] = ownpos[2] + ( pos[1] * multiplier)

    Htranslate.localPosition = transpos

    camzorientation = Hcamera.localOrientation.to_euler()[2]
    camxorientation = Hcamera.localOrientation.to_euler()[0]
    camyorientation = Hcamera.localOrientation.to_euler()[1]

    own['rotz'] = camzorientation
    own['rotx'] = camxorientation
    own['roty'] = camyorientation
    
    
    
    
#data sample from vrpnOpenVR bridge : {'time': datetime.datetime(2018, 5, 13, 14, 28, 14, 853502), 'sensor': 0, 'quaternion': (0.39259548331500704, -0.11693821857682538, 0.07490279314799107, 0.909166551079176), 'position': (0.2786966562271118, 1.3260390758514404, -0.549099862575531)}


